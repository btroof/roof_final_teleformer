﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RevealTextOverTime : MonoBehaviour
{
    // Reference
    private TextMeshPro TMP;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        // Setting up reference.
        TMP = gameObject.GetComponent<TextMeshPro>();

        // Get num of char in text object.
        int totalVisibleCharacters = TMP.textInfo.characterCount;
        int counter = 0;

        while (true)
        {
            int visibleCount = counter % (totalVisibleCharacters + 1);

            // How many chars TMP should show,.
            TMP.maxVisibleCharacters = visibleCount;
            
            // Increment counter if counter is less than TMP text count.
            if (counter < TMP.textInfo.characterCount)
            {
                counter += 1;
            }

            // Time to delay before displaying the next letter.
            yield return new WaitForSeconds(0.04f);
        }
    }
}
