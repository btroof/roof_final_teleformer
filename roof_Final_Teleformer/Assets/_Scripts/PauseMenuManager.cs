﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.PostProcessing;

public class PauseMenuManager : MonoBehaviour
{
    // References
    public PostProcessingProfile ppProfile;
    VignetteModel.Settings vignetteSettings;
    public GameObject pauseMenu;
    public GameObject pausedTextObject;
    public GameObject controlsPanel;
    GameObject player;
    public AudioClip enter;
    public AudioClip exit;
    AudioSource audioSource;
    // Variables
    public bool isPaused = false;

    // Start is called before the first frame update
    void Start()
    {
        // Setting up references
        audioSource = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player");
        // Initializing variables
        vignetteSettings = ppProfile.vignette.settings;
        vignetteSettings.intensity = 0.25f;
        ppProfile.vignette.settings = vignetteSettings;
    }

    // Update is called once per frame
    void Update()
    {

        // Calls SetMenu function when the enter key pressed.
        if (Input.GetKeyDown(KeyCode.Return)) 
        {
            SetMenu();
        }
    }

    // Function used to unpause the game, hide the pause menu and reset vignette settings.
    public void Resume()
    {        
        Time.timeScale = 1;
        vignetteSettings.intensity = 0.25f;
        ppProfile.vignette.settings = vignetteSettings;
        player.GetComponent<FirstPersonController>().isPaused = false;
        isPaused = false;
        pausedTextObject.SetActive(true);
        controlsPanel.SetActive(false);
        pauseMenu.SetActive(false);
    }

    // Function used to pause the game, show the pause menu and set vignette settings.
    void Pause()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
        vignetteSettings.intensity = 0.4f;
        ppProfile.vignette.settings = vignetteSettings;
        player.GetComponent<FirstPersonController>().isPaused = true;
        isPaused = true;
    }

    // Called when the Enter key is pressed..
    public void SetMenu()
    {
        // If the game is paused call the Resume function and play the exit menu sound.
        if (isPaused)
        {
            Resume();
            audioSource.clip = exit;
            audioSource.Play();
        }

        // Otherwise call the Pause function and play the enter menu sound.
        else
        {
            Pause();
            audioSource.clip = enter;
            audioSource.Play();
        }
    }
}