﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Projectile : MonoBehaviour
{
    // References
    public GameObject aura;
    public GameObject player;
    public GameObject launcher;
    // Variables
    public bool needToTeleport;
    float scaleX;
    float scaleY;
    float scaleZ;

    // Start is called before the first frame update
    private void Awake()
    {
        // Setting up references.
        player = GameObject.FindGameObjectWithTag("Player");
        launcher = GameObject.FindGameObjectWithTag("Launcher");
        aura = GameObject.FindGameObjectWithTag("Aura");
        // Initilizing Variables
        scaleX = 1.8f;
        scaleY = 1.8f;
        scaleZ = 1.8f;
    }

    private void Update()
    {
        // If the launcer is charging...
        if (launcher.GetComponent<Launcher>().charging)
        {
            // increase the scale values for the aura object.
            if (scaleX < 3f)
            {
                scaleX += 0.75f * Time.deltaTime;
                scaleY += 0.75f * Time.deltaTime;
                scaleZ += 0.75f * Time.deltaTime;
            }
        }
        // otherwise reset them.
        else
        {
            scaleX = 1.8f;
            scaleY = 1.8f;
            scaleZ = 1.8f;
        }        
    }
void LateUpdate()
    {
        // If the launcer is charging the projectile should remain fixed to the barrel.
        if (launcher.GetComponent<Launcher>().charging)
        {
            transform.position = launcher.transform.position;
        }
    }

    void FixedUpdate()
    {
        // If the aura object exists, set its scale to the provided values.
        if (aura.gameObject != null)
        {
        aura.GetComponent<Transform>().localScale = new Vector3 (scaleX, scaleY, scaleZ);
        }

        // if the projectile gets to far from the player, allow recharge and destroy the projectile.
        if (Vector3.Distance(player.transform.position, transform.position) > 25f)
        {
            launcher.GetComponent<Launcher>().canShoot = true;
            launcher.GetComponent<Launcher>().projectileExists = false;
            Destroy(gameObject);
        }
    }

    // Checks for collision.
    private void OnCollisionEnter(Collision other)
    {
        // If the object is the floor, pass the projectiles location to the first person controller.
        // Then allow the recharge meter to charge and resset the launch power as the projectile should
        // no longer exist so this object should be destroyed.
        if (other.gameObject.CompareTag("Floor") && !launcher.GetComponent<Launcher>().charging)
        {
            player.GetComponent<FirstPersonController>().teleportToLocationX = transform.position.x;
            player.GetComponent<FirstPersonController>().teleportToLocationZ = transform.position.z;
            player.GetComponent<FirstPersonController>().teleportToLocationY = transform.position.y;
            player.GetComponent<FirstPersonController>().needToTeleport = true;
            launcher.GetComponent<Launcher>().canShoot = true;
            launcher.GetComponent<Launcher>().projectileExists = false;
            launcher.GetComponent<Launcher>().launchPower = 1;
            Destroy(gameObject);
            
        }

        // If the object is the plasma pool/ allow the recarge meter to charge and resset the launch power as the 
        // projectile should no longer exist so this object should be destroyed.
        if (gameObject.CompareTag("Projectile") && other.gameObject.CompareTag("Plasma") && !launcher.GetComponent<Launcher>().charging)
        {
            launcher.GetComponent<Launcher>().canShoot = true;
            launcher.GetComponent<Launcher>().projectileExists = false;
            launcher.GetComponent<Launcher>().launchPower = 1;
            Destroy(gameObject);
        }
    }
}
