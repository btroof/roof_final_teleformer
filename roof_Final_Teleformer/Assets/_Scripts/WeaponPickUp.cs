﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickUp : MonoBehaviour
{
    // References
    AudioSource audioSource;
    GameObject gameController;
    GameObject gun;
    GameObject ammo;
    GameObject battery1;
    GameObject battery2;
    GameObject launcher;
    // Variables
    public bool entered;
    public float destroyTimer = 1;

    // Start is called before the first frame update
    void Start()
    {
        // Setting up references.
        audioSource = GetComponent<AudioSource>();
        gun = GameObject.FindGameObjectWithTag("Gun");
        gameController = GameObject.FindGameObjectWithTag("GameController");
        ammo = GameObject.FindGameObjectWithTag("Ammo");
        battery1 = GameObject.FindGameObjectWithTag("Battery1");
        battery2 = GameObject.FindGameObjectWithTag("Battery2");
        launcher = GameObject.FindGameObjectWithTag("Launcher");
        // Initilizing Variables
        entered = false;
        gun.GetComponent<MeshRenderer>().enabled = false;
        ammo.GetComponent<MeshRenderer>().enabled = false;
        battery1.GetComponent<MeshRenderer>().enabled = false;
        battery2.GetComponent<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        // if the player has picked up the weapon an decrement the timer...
        if (entered)
        {
            destroyTimer -= Time.deltaTime;
            // ... When the time hits 0, enable the players gun parts and then destroy this object.
            if (destroyTimer <= 0f)
            {
                gun.GetComponent<MeshRenderer>().enabled = true;
                ammo.GetComponent<MeshRenderer>().enabled = true;
                battery1.GetComponent<MeshRenderer>().enabled = true;
                battery2.GetComponent<MeshRenderer>().enabled = true;
                launcher.GetComponent<Launcher>().canShoot = true;
                gameController.GetComponent<GameController>().weaponPickedUp = true;
                Destroy(gameObject);
            }
        }

        // when the game controller shows this gun has been picked up, the players gun shoud always be shown and this one should be destroyed.
        if (gameController.GetComponent<GameController>().weaponPickedUp == true)
        {
            gun.GetComponent<MeshRenderer>().enabled = true;
            ammo.GetComponent<MeshRenderer>().enabled = true;
            battery1.GetComponent<MeshRenderer>().enabled = true;
            battery2.GetComponent<MeshRenderer>().enabled = true;
            launcher.GetComponent<Launcher>().canShoot = true;
            Destroy(gameObject);
        }
    }

    // Checks for collision.
    private void OnTriggerEnter(Collider other)
    {
        // if object is the player, play pick up sound if the player has not already entered and set entered true.
        if (other.CompareTag("Player"))
        {
            if (!entered)
            {
                audioSource.Play();
            }
            entered = true;
        }
    }
}