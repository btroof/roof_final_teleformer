﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CompletedLevel : MonoBehaviour
{
    // References
    GameObject gameController;
    // Start is called before the first frame update
    void Start()
    {
        // Setting up references.
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    // Checks for collision.
    private void OnTriggerEnter(Collider other)
    {
        // If the ojecg is the player, the game is over and the player won. Show tutorial complete scene.
        if (other.gameObject.CompareTag("Player"))
        {
            gameController.GetComponent<GameController>().gameOver = true;
            Destroy(gameController);
            SceneManager.LoadScene("roof_TutorialCompleteScreen_Teleformer");
        }
    }
}
