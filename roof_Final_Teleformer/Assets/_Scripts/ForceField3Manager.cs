﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ForceField3Manager : MonoBehaviour
{
    // References
    GameObject launcher;
    GameObject gameController;
    GameObject checkpointPrompt;
    public GameObject checkpointCollider_3;
    // Variables
    float promptTimer;
    bool promptSet;
    // Start is called before the first frame update
    void Start()
    {
        // Setting up references
        checkpointPrompt = GameObject.FindGameObjectWithTag("CheckpointPrompt");
        launcher = GameObject.FindGameObjectWithTag("Launcher");
        gameController = GameObject.FindGameObjectWithTag("GameController");

        // Initializing variables.
        promptTimer = 0;
        promptSet = false;

        // If the third checkpoint has been hit, destroy the force field for it and do not display the checkpoint text. 
        if (gameController.GetComponent<GameController>().checkpointHit3)
        {
            checkpointPrompt.GetComponent<TextMeshProUGUI>().text = " ";
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // If the prompt timer has a value, decrement it over time and set the var to allow it to be displayed.
        if (promptTimer > 0)
        {
            promptSet = false;
            promptTimer -= Time.deltaTime;
        }

        // If the prompt has been set and the timer has reached 0, stop displaying it.
        if (!promptSet && gameController.GetComponent<GameController>().checkpointHit3 && promptTimer <= 0)
        {
            checkpointPrompt.GetComponent<TextMeshProUGUI>().text = " ";
            promptSet = true;
        }
    }

    // Checks for collisions with other objects.
    private void OnTriggerEnter(Collider other)
    {
        // ... if the player has not already been through the third checkpoint, 
        // disable the force field, set the prompt, its timer, and the game controllers checkpoint 3 var.
        if (other.CompareTag("Player") && !gameController.GetComponent<GameController>().checkpointHit3)
        {
            checkpointPrompt.GetComponent<TextMeshProUGUI>().text = "CHECKPOINT";
            promptTimer = 3;
            gameController.GetComponent<GameController>().checkpointHit3 = true;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            Destroy(checkpointCollider_3);
        }
    }
}
