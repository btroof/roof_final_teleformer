﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    // References
    public GameObject launcher;
    // Variables
    public float hSliderValue = -0.3F;
    float lightRange;

    // Start is called before the first frame update
    private void Start()
    {
        // Setting up references.
        launcher = GameObject.FindGameObjectWithTag("Launcher");
        // Initilizing Variables
        lightRange = GetComponent<Light>().range;
    }

    private void Update()
    {
        var main = GetComponent<ParticleSystem>().main;
        main.simulationSpeed = hSliderValue;

        // If charging and the light has reset, increase the range
        if (lightRange < 0.7f && launcher.GetComponent<Launcher>().charging)
        {
            lightRange += 0.28f * Time.deltaTime;
        }
        if (!launcher.GetComponent<Launcher>().charging)
        {
            lightRange = 0.2f;
        }
        GetComponent<Light>().range = lightRange;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        // if charging, emit particles.
        if (launcher.GetComponent<Launcher>().charging)
        {
            var emmision = GetComponent<ParticleSystem>().emission;
            
            emmision.enabled = true;

            if (hSliderValue < 3)
            {
                hSliderValue += 0.25f;
            }          
        }
        // Otherwise hide them.
        else
        {
            var emmision = GetComponent<ParticleSystem>().emission;
            emmision.enabled = false;
            GetComponent<ParticleSystem>().Stop();
        }
    }
}