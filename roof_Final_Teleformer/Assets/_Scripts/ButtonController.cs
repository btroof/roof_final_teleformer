﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    // References
    public Text loading;
    public GameObject ControlsPanel;
    public GameObject MainPanel;
    GameObject gameController;

    private void Start()
    {
        // Setting up reference on start
        gameController = GameObject.FindGameObjectWithTag("GameController");
        // Initializing Variable
        if (loading != null)
        {
            loading.text = " ";
        }
    }

    // Function that loads the main scene and forces the gamecontroller to reset before it is destroyed. 
    // This function is called in by a button UI and displays loading text.
    public void LoadMainScene()
    {
        //checks if the gamcontroller exists when called. 
        //Sets vars in the game controller that handle the game's state to ensure a clean restart of the scene.
        if (gameController != null)
        {
            gameController.GetComponent<GameController>().timerNotSet = true;
            gameController.GetComponent<GameController>().gameOver = true;
        }
        loading.text = "Loading...";
        Destroy(gameController);
        SceneManager.LoadScene("roof_Final_Teleformer");
    }

    // Function that loads the main scene and forces the gamecontroller to reset before it is destroyed. 
    // This function is called in by a button UI and displays loading text.
    public void LoadIntroScene()
    {
        //checks if the gamcontroller exists when called. 
        //Sets vars in the game controller that handle the game's state to ensure a clean restart of the scene.
        if (gameController != null)
        {
            gameController.GetComponent<GameController>().timerNotSet = true;
            gameController.GetComponent<GameController>().gameOver = true;
        }
        loading.text = "Loading...";
        Destroy(gameController);
        SceneManager.LoadScene("roof_Introduction_Teleformer");
    }
    // Function that loads the menu scene and forces the gamecontroller to reset before it is destroyed. 
    // This function is called in by a button UI and displays loading text.
    public void LoadMenuScene()
    {
        // checks if the gamcontroller exists when called. 
        //Sets vars in the game controller that handle the game's state to ensure a clean restart of the scene.
        if (gameController != null)
        {
            gameController.GetComponent<GameController>().timerNotSet = true;
            gameController.GetComponent<GameController>().gameOver = true;
        }
        loading.text = "Loading...";                
        Destroy(gameController);
        SceneManager.LoadScene("roof_Menu_Teleformer");
    }

    // Function that exits the application. Called in by a button UI.
    public void QuitGame()
    {
        Application.Quit();
    }

    // Function to hide the main panel and show the controls panel. Called by button UI.
    public void ShowControls()
    {
        ControlsPanel.SetActive(true);
        MainPanel.SetActive(false);
    }

    // Function to show the main panel and hide the controls panel. Called by button UI.
    public void HideControls()
    {
        MainPanel.SetActive(true);
        ControlsPanel.SetActive(false);
    }
}
