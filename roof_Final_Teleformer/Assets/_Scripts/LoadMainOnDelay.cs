﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadMainOnDelay : MonoBehaviour
{
    public Text loading;
    float delayTimer;
    // Start is called before the first frame update
    void Start()
    {
        delayTimer = 25.0f;
        // Initializing Variable
        if (loading != null)
        {
            loading.text = " ";
        }
    }

    // Update is called once per frame
    void Update()
    {

        delayTimer -= Time.deltaTime;

        // call function to load main scene when timer reaches 0;
        if (delayTimer <= 0.0f)
        {
            LoadMainScene();
        }
    }


    // Function that loads the main scene and forces the gamecontroller to reset before it is destroyed. 
    // This function is called in by a button UI and displays loading text.
    public void LoadMainScene()
    {
        loading.text = "Loading...";
        SceneManager.LoadScene("roof_Final_Teleformer");
    }
}
