﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFakeRotator : MonoBehaviour
{
    void Update()
    {
        // Rotates the projectile fake when it is active.
        transform.Rotate((new Vector3(360, 30, 360) * Time.deltaTime) * 4);
    }
}
