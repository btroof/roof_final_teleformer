﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ForceField1Manager : MonoBehaviour
{
    // References
    GameObject launcher;
    GameObject gameController;
    GameObject checkpointPrompt;
    public GameObject checkpointCollider_1;
    public AudioClip error;

    // Variables
    float promptTimer;
    bool promptSet;
    // Start is called before the first frame update
    void Start()
    {
        // Setting up references.
        checkpointPrompt = GameObject.FindGameObjectWithTag("CheckpointPrompt");
        launcher = GameObject.FindGameObjectWithTag("Launcher");
        gameController = GameObject.FindGameObjectWithTag("GameController");

        // Initializing variables.
        promptTimer = 0;
        promptSet = false;

        // If the first checkpoint has been hit, destroy the force field for it and do not display the checkpoint text.. 
        if (gameController.GetComponent<GameController>().checkpointHit1)
        {
            checkpointPrompt.GetComponent<TextMeshProUGUI>().text = " ";
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // If the prompt timer has a value, decrement it over time and set the var to allow it to be displayed.
        if (promptTimer > 0)
        {
            promptTimer -= Time.deltaTime;
            promptSet = false;
        }
        // If the prompt has been set and the timer has reached 0, stop displaying it.
        if (!promptSet && gameController.GetComponent<GameController>().checkpointHit1 && promptTimer <= 0)
        {
            checkpointPrompt.GetComponent<TextMeshProUGUI>().text = " ";
            promptSet = true;
        }
    }

    // Checks for collisions with other objects.
    private void OnTriggerEnter(Collider other)
    {
        // if the other object is the player...
        if (other.CompareTag("Player"))
        {
            // ... if the player has not picked up the gun and has not already been through the first checkpoint, 
            // display a message and play the error sound.
            if (!launcher.GetComponent<Launcher>().canShoot && !gameController.GetComponent<GameController>().checkpointHit1)
            {
                checkpointPrompt.GetComponent<TextMeshProUGUI>().text = "KEY REQUIRED";
                GetComponent<AudioSource>().clip = error;
                GetComponent<AudioSource>().Play();
            }

            // ... if the player has picked up the gun and has not already been through the first checkpoint, 
            // disable the force field, set the prompt, its timer, and the game controllers checkpoint 1 var.
            if (launcher.GetComponent<Launcher>().canShoot && !gameController.GetComponent<GameController>().checkpointHit1)
            {
                checkpointPrompt.GetComponent<TextMeshProUGUI>().text = "CHECKPOINT";
                gameController.GetComponent<GameController>().checkpointHit1 = true;
                gameObject.GetComponent<MeshRenderer>().enabled = false;
                promptTimer = 3;
                Destroy(checkpointCollider_1);
            }
        }
    }
    
    // Checks for when collided objects leave the field.
    private void OnTriggerExit(Collider other)
    {
        // If the object is the player and the player has not picked up the weapon, clear the prompt text.
        if (other.CompareTag("Player") && !gameController.GetComponent<GameController>().checkpointHit1)
        {
            checkpointPrompt.GetComponent<TextMeshProUGUI>().text = " ";
        }
    }
}
