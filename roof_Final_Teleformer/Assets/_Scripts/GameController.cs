﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // Referennces
    public static GameController gc;
    public GameObject music;
    public GameObject timerText;
    public GameObject livesText;
    // Variables
    public static float gameTimer = 600f;
    public float checkpointPoolLevel = -15.25f;
    public float promptTimer = 10;
    public int lives = 9;  
    public bool checkpointHit1 = false;
    public bool checkpointHit2 = false;
    public bool checkpointHit3 = false;
    public bool gameOver = false;
    public bool CP1SoundPlayed = false;
    public bool CP2SoundPlayed = false;
    public bool CP3SoundPlayed = false;
    public bool weaponPickedUp = false;
    public bool showedPrompt = false;
    public bool timerNotSet = true;
    public float timeLeft;
    public float savedTime;
    
    void Awake()
    {
        // Singelton pattern, if a don't destroy object does not exist and the game is not over, 
        // make the game object holding this script it.
        if (gc == null && !gameOver)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
        }
        else // Destroy this game object.
        {            
            Destroy(gameObject);
            return;
        }
    }

    void Update()
    {
        // If the timer has not been set, set pool level and timer to their initial values.
        // then set the timerNotSet var to false. Timer has been set.
        if (timerNotSet)
        {
            checkpointPoolLevel = -15.25f;
            timeLeft = gameTimer;
            timerNotSet = false;
        }

        // Updates lives left text UI.
        livesText.GetComponent<Text>().text = "Lives: 0" + lives.ToString();

        // If the game is not over this statements allows the timer to countdown and be formatted.
        if (timeLeft > 0)
        {
            // decrements the timeLeft time var.
            timeLeft -= Time.deltaTime;

            // converts countdown time to minutes.
            int minutes = Mathf.FloorToInt(timeLeft / 60F);

            // converts countdown var to seconds.
            int seconds = Mathf.FloorToInt(timeLeft - minutes * 60);

            // combines minutes and seconds for UI display.
            string formattedTime = string.Format("{00:00}:{01:00}", minutes, seconds);

            // assigns formattedTime var to timerText's text property.
            timerText.GetComponent<Text>().text = formattedTime;
        }

        // If the game is over, stop the background music for this scene.
        if (gameOver)
        {
            music.GetComponent<AudioSource>().Stop();
        }
        
        // If there is time in the prompt timer, countdown time until it reaches 0.
        if (promptTimer > 0)
        {
            promptTimer -= Time.deltaTime;
        }

        // If the prompt timer reaches 0, the prompt has been shown.
        if (promptTimer <= 0)
        {
            showedPrompt = true;
        }    

        // If player lives of the game timer reach 0, the game is over, load the game over scene and destroy the gameController.
        if (lives <= 0 || timeLeft <= 0.1)
        {
            gameOver = true;
            SceneManager.LoadScene("roof_GameOverScreen_Teleformer");
            Destroy(gameObject);
        }

        // If the first checkpoint has been hit and the checkpoint sound has not played,
        // play the sound and set the pool level, then the sound has been played and should not repeat.
        if (checkpointHit1 && !CP1SoundPlayed)
        {
            savedTime = timeLeft;
            GetComponent<AudioSource>().Play();
            checkpointPoolLevel = PoolManager.newY;
            CP1SoundPlayed = true;
        }

        // If the second checkpoint has been hit and the checkpoint sound has not played,
        // play the sound and set the pool level, then the sound has been played and should not repeat.
        if (checkpointHit2 && !CP2SoundPlayed)
        {
            savedTime = timeLeft;
            GetComponent<AudioSource>().Play();
            checkpointPoolLevel = PoolManager.newY;
            CP2SoundPlayed = true;
        }

        // If the third checkpoint has been hit and the checkpoint sound has not played,
        // play the sound and set the pool level, then the sound has been played and should not repeat.
        if (checkpointHit3 && !CP3SoundPlayed)
        {
            savedTime = timeLeft;
            GetComponent<AudioSource>().Play();
            checkpointPoolLevel = PoolManager.newY;
            CP3SoundPlayed = true;
        }
    }
}
