﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Launcher : MonoBehaviour
{
    // References
    public GameObject aura;
    GameObject pauseMenu;
    public GameObject gameController;
    public GameObject projectileFake;
    public Rigidbody projectile;
    GameObject cloneFake;
    Rigidbody clone;
    public AudioClip chargeSound;
    public AudioClip shootingSound;
    public AudioClip rechargeSound;
    public AudioSource audioSource;
    public Slider rechargeMeter;
    // Variables
    bool chargeReset;
    bool released;
    bool waitForResume;
    bool shootingSoundPlayed;
    bool paused;
    public bool canShoot;
    public bool charging;
    public bool projectileExists;
    public bool recharged;
    public float launchModifier;
    public float launchMax;
    public float powerTimer;
    public float launchPower;
    public float rechargeTimer;
    public Vector3 teleportLocation;

    void Start()
    {
        // Setting up references.
        gameController = GameObject.FindGameObjectWithTag("GameController");
        audioSource = GetComponent<AudioSource>();
        aura = GameObject.FindGameObjectWithTag("Aura");

        // Initilizing Variables
        pauseMenu = GameObject.FindGameObjectWithTag("PauseManager");
        paused = pauseMenu.GetComponent<PauseMenuManager>().isPaused;
        chargeReset = false;
        recharged = true;
        canShoot = false;
        released = false;
        waitForResume = false;
        shootingSoundPlayed = false;
        rechargeTimer = 3f;
        launchMax = 16f;
        launchModifier = 8;
        powerTimer = 0f;
        launchPower = 1f;
    }
    // Update is called once per frame
    void Update()
    {
        paused = pauseMenu.GetComponent<PauseMenuManager>().isPaused;

        // If the charge is reset and the volume is less than 1, modulate pitch and volume.
        if (chargeReset && audioSource.volume < 1)
        {
            audioSource.volume += 0.4f * Time.deltaTime;
            audioSource.pitch += 0.8f * Time.deltaTime;
        }
        
        // if the launch power is less than the launch max and it is charging, increment power timer.
        if (launchPower < launchMax && charging)
        {
            powerTimer += Time.deltaTime;
        }

        // set launch power times the modifier value.
        launchPower = powerTimer * launchModifier;
        
        // If not charging, power timer should be 0 and the projectile fake should destroyed.
        if (!charging)
        {
            powerTimer = 0f;
            Destroy(GameObject.FindGameObjectWithTag("ProjectileFake"));
        }

        //if the recharge timer is full, the gun has recharged.
        if (rechargeTimer >= 3f)
        {
            recharged = true;
        }

        // If left click and the weapon has been picked up...
        if (Input.GetMouseButtonDown(0) && gameController.GetComponent<GameController>().weaponPickedUp)
        {
            // ... and if the player cannot shoot of the gun is recharging, play the needs to recharge sound
            if (!canShoot || rechargeTimer < 3f)
            {
                PlayNeedRechargeSound();
            }
        }

        // If the player can shoot, increase the recharge timer...
        if (canShoot)
        {
            rechargeTimer += Time.deltaTime;

            // ... If the recharge timer is complete, allow the player to fire.
            if (recharged)
            {
                Fire();
            }
        }

        // If the game is paused, wait to resume, else dont wait.
        if (paused)
        {
            waitForResume = true;
        }
        else
        {
            waitForResume = false;
        }    

        // If the game is pause any gun sfx should stop.
        if (paused)
        {
            audioSource.Stop();
            audioSource.loop = false;
            audioSource.clip = null;
        }

        // If left click has been released, the player can shoot, and not waiting, the player has fired and the
        // shooting sound should play. 
        if (released && canShoot && !waitForResume)
        {
            shootingSoundPlayed = false;
            audioSource.Stop();
            Destroy(cloneFake);
            clone = Instantiate(projectile, transform.position, transform.rotation);
            clone.velocity = transform.TransformDirection(Vector3.forward * launchPower);
            canShoot = false;
            charging = false;
            rechargeTimer = 0f;
            recharged = false;
            released = false;
        }
        
        // If not waiting to resume nor paused, the shooting sounnd hasn't played and the projectile clone is not null,
        // play the shooting sound once.
        if (!waitForResume && !paused && !shootingSoundPlayed && clone != null)
        {
            PlayShootingSound();
            shootingSoundPlayed = true;
        }

        rechargeMeter.value = rechargeTimer/3f;
    }

    // Called in update when the player can shoot and the gun is recharged.
    void Fire()
    {
        // If the left mouse is down and the game is not paused, create a clone of the 
        // projectile prefab and launch it forward on the z axiz of this object...
        if (Input.GetMouseButtonDown(0) && !paused)
        {
            cloneFake = Instantiate(projectileFake, transform.position, transform.rotation);
            cloneFake.transform.Rotate(new Vector3(45, 30, 45) * Time.deltaTime);

            // If the charging sound needs reset, do so and set the charge reset var true.
            if (!chargeReset && audioSource.volume == 1)
            {
                audioSource.volume = 0.3f;
                audioSource.pitch = 0.1f;
                chargeReset = true;
            }
            
            // Set charging true when this button is held down.
            charging = true;

            //if the game is KeyNotFoundException paused, call the charging sound function.
            if (!paused)
            {
                PlayChargingSound();
            }
            
        }

        // if left click has bee released and the clone fake exists, show this button has been released.
        if (Input.GetMouseButtonUp(0) && cloneFake != null)
        {
            released = true;
        }

    }

    // Function to play the shooting sound, resets values before playing.
    private void PlayShootingSound()
    {
        audioSource.volume = 1f;
        audioSource.pitch = 1;
        audioSource.Stop();
        audioSource.PlayOneShot(shootingSound, 1f);
        audioSource.loop = false;
        chargeReset = false;
    }

    // Function to play and modulate the charging sound.
    private void PlayChargingSound()
    {
        audioSource.Stop();
        audioSource.clip = chargeSound;
        audioSource.loop = true;
        audioSource.Play();
    }

    // Function to play the need to allow recharge sound, resets values before playing.
    private void PlayNeedRechargeSound()
    {
        audioSource.volume = 1;
        audioSource.pitch = 1;
        audioSource.Stop();
        audioSource.clip = rechargeSound;
        audioSource.loop = false;
        chargeReset = false;
        audioSource.PlayOneShot(rechargeSound, 1f);
    }
}