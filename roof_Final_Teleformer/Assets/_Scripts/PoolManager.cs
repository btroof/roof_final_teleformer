﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;

public class PoolManager : MonoBehaviour
{
    // References
    public PostProcessingProfile ppProfile;
    ColorGradingModel.Settings cgSettings;
    public GameObject launcher;
    GameObject gameController;
    public GameObject player;
    
    // Variables
    float playerY;
    public static float newY;
    float killTimer;
    bool kill;

    // Start is called before the first frame update
    void Start()
    {
        // Setting up references
        gameController = GameObject.FindGameObjectWithTag("GameController");
        playerY = player.GetComponent<Transform>().position.y;
        launcher = GameObject.FindGameObjectWithTag("Launcher");
        // initializing variables
        newY = gameController.GetComponent<GameController>().checkpointPoolLevel;
        kill = false;
        cgSettings = ppProfile.colorGrading.settings;
        cgSettings.basic.temperature = 0f;
        cgSettings.basic.tint = 0f;
        cgSettings.basic.contrast = 1f;
        ppProfile.colorGrading.settings = cgSettings;
    }

    // Update is called once per frame
    void Update()
    {
        // If the player is below the plasma pool, change the color of the screen to a florecent blue.
        if (player.transform.position.y < transform.position.y - 0.5)
        {
            cgSettings.basic.temperature = -100f;
            cgSettings.basic.tint = 100f;
            cgSettings.basic.contrast = 0.3f;
            ppProfile.colorGrading.settings = cgSettings;
        }

        // If the kill timer has a value greater than or equal to 0, decrement it over time.
        if (killTimer >= 0)
        {
            killTimer -= Time.deltaTime;
        }

        // If the kill timer has a value less than 0 and the kill var is true, call the Kill() function.
        if (killTimer < 0 && kill)
        {
            Kill();
        }

        // Set the pools new position over time so that the timer expires as the player runs out of surfaces they may stand on.
        newY += (1 / (GameController.gameTimer / 107)) * Time.deltaTime;

        // Update the pool position every frame.
        transform.position = new Vector3(31.9f, newY, 0f);
    }

    // Check for collision of objects with the pool.
    private void OnTriggerEnter(Collider other)
    {
        // If the object is a projectile, it allows the player recharge meter to start and destroys the projectile
        if (other.CompareTag("Projectile"))
        {
            launcher.GetComponent<Launcher>().canShoot = true;
            launcher.GetComponent<Launcher>().projectileExists = false;
            Destroy(other.gameObject);
        }
        // If the object is the player, it plays the death sound at a random pitch, sets the kill timer, and sets the kill var true.
        if (other.CompareTag("Player"))
        {
            GetComponent<AudioSource>().pitch = Random.Range(0.7f, 1f);
            GetComponent<AudioSource>().Play();
            killTimer = 0.5f;
            kill = true;
        }
    }

    // Function to decrement player lives and reload the main scene if lives is greater than 0, else load game over scene when called.
    void Kill()
    {
        int lives = gameController.GetComponent<GameController>().lives -= 1;
        if (lives > 0)
        {
            SceneManager.LoadScene("roof_Final_Teleformer");
        }
        else
        {
            SceneManager.LoadScene("roof_GameOverScreen_Teleformer");
            Destroy(gameController);
        }
    }
}
