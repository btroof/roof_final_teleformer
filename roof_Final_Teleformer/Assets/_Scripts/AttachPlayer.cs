﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachPlayer : MonoBehaviour
{
    // Reference
    private Transform tr = null;

    // Checks for collision trigger.
    void OnTriggerEnter(Collider collider)
    {
        // If the object is the player, set the player as a child to the object it collided with.
        if (collider.CompareTag("Player"))
        {
            tr = collider.gameObject.transform.parent;
            collider.gameObject.transform.SetParent(transform);
        }
    }

    // On collision trigger exit.. 
    void OnTriggerExit(Collider collider)
    {
        // If the object is the player, set its parent to null.
        if (collider.CompareTag("Player"))
        {
            collider.gameObject.transform.SetParent(null);
        }
    }
}

