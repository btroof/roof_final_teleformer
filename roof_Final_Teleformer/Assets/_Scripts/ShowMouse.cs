﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMouse : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Unlocks the cursor on start for menu scenes.
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
}
