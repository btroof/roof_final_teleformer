﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class CheckpointController : MonoBehaviour
{
    // References
    GameObject player;
    GameObject gameController;
    GameObject plasmaPool;
    // Variables
    bool gameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        // Setting up references.
        plasmaPool = GameObject.FindGameObjectWithTag("Plasma");
        player = GameObject.FindGameObjectWithTag("Player");
        gameController = GameObject.FindGameObjectWithTag("GameController");

        // if the player has not hit a check point, restart the game timer when starting the scene.
        if (!gameController.GetComponent<GameController>().checkpointHit1 && !gameController.GetComponent<GameController>().checkpointHit2 && !gameController.GetComponent<GameController>().checkpointHit3)
        {
            gameController.GetComponent<GameController>().timerNotSet = true;
        }

        // If the player has hit a check point, they should spawn there unitl the checkpoint is not valid any longer.
        if (gameController.GetComponent<GameController>().checkpointHit1)
        {
            player.GetComponent<FirstPersonController>().checkpoint1 = true;
            player.GetComponent<FirstPersonController>().checkpoint2 = false;
            player.GetComponent<FirstPersonController>().checkpoint3 = false;
            gameController.GetComponent<GameController>().timeLeft = gameController.GetComponent<GameController>().savedTime;
        }
        
        if (gameController.GetComponent<GameController>().checkpointHit2 && !gameOver)
        {
            player.GetComponent<FirstPersonController>().checkpoint1 = false;
            player.GetComponent<FirstPersonController>().checkpoint2 = true;
            player.GetComponent<FirstPersonController>().checkpoint3 = false;
            gameController.GetComponent<GameController>().timeLeft = gameController.GetComponent<GameController>().savedTime;
        }

        
        if (gameController.GetComponent<GameController>().checkpointHit3 && !gameOver)
        {
            player.GetComponent<FirstPersonController>().checkpoint1 = false;
            player.GetComponent<FirstPersonController>().checkpoint2 = false;
            player.GetComponent<FirstPersonController>().checkpoint3 = true;
            gameController.GetComponent<GameController>().timeLeft = gameController.GetComponent<GameController>().savedTime;
        }

    }
}
