﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanRotationManager : MonoBehaviour
{
    void Update()
    {
        // Rotates fan blades.
        transform.Rotate(new Vector3(0, 0, -45) * Time.deltaTime);
    }
}
