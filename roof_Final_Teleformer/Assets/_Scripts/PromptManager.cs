﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromptManager : MonoBehaviour
{
    // References
    public GameObject pausePrompt;
    public GameObject pauseManager;
    GameObject gameController;
    // Start is called before the first frame update
    void Start()
    {
        // Setting up references.
        gameController = GameObject.FindGameObjectWithTag("GameController");

        // Show prompt on start.
        if (!gameController.GetComponent<GameController>().showedPrompt)
        {
            pausePrompt.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // If the pause manager is not null and the game is paused and prompt has not been shown, show it, else disable it.
        if(pauseManager != null && !pauseManager.GetComponent<PauseMenuManager>().isPaused && !gameController.GetComponent<GameController>().showedPrompt)
        {
            pausePrompt.SetActive(true);
        }
        else if(pauseManager.GetComponent<PauseMenuManager>().isPaused || gameController.GetComponent<GameController>().showedPrompt)
        {
            pausePrompt.SetActive(false);
        }
    }
}
